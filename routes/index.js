const express = require('express');
const router = express.Router();
const env = process.env.NODE_ENV || 'development';
const knexConfig = require('../knexfile');
const knex = require('knex')(knexConfig[env]);

/* GET home page. */
router.get('/', async (req, res, next) => {
  const clouds = await knex('clouds');
  res.render('index', { title: 'CloudApp', clouds });
});

module.exports = router;

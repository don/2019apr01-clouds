
exports.up = function(knex, Promise) {
  return knex.schema.createTable('clouds', t => {
    t.increments();
    t.string('name');
    t.string('type');
    t.integer('size');
    t.timestamps(true, true);
  });  
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('clouds'); 
};
